#!/usr/bin/env bats

load ../src/fakes

setup() {
    cleanupfakes
}

@test "creates the fake" {
    fake foobar

    test -x $BATS_TEST_DIRNAME/.fakes/foobar
}

@test "the fake prints the provided output" {
    fake foobar "--some-arguments" "the output"

    foobar

    [ "$(foobar)" = "the output" ]
}

@test "definint a fake creates the expected arguments file" {
    fake foobar "--one-arg --two-args" "the output"
    echo "--one-arg --two-args" > "$BATS_TEST_DIRNAME/.fakes/.calls/foobar.hopefully"
    
    echo "--one-arg --two-args" | cmp "$BATS_TEST_DIRNAME/.fakes/.calls/foobar.expected" -
}

@test "cleanupfakes cleans up the fakes dir" {
    fake baz

    test -d $BATS_TEST_DIRNAME/.fakes

    cleanupfakes

    test ! -d $BATS_TEST_DIRNAME/.fakes
}

@test "running the fake records calls to the actual arguments file" {
    fake foobar

    foobar -a --bee -c

    echo "-a --bee -c" | cmp "$BATS_TEST_DIRNAME/.fakes/.calls/foobar.actual" -
}
