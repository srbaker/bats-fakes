# Fakes for BATS

Fakes for BATS is a library for faking commands in your BATS tests.

## Usage

``` shell
load fakes

@test "fake a command" {
    fake date "date --utc" "the_date"
    
    run command_that_calls_date
    
    verify date
}
```
