#!/usr/bin/env bash

# Fakes for BATS
# Copyright (c) 2016 Steven R. Baker <steven@stevenrbaker.com>
# Released under the LGPLv3+. See COPYING.LESSER for details.

FAKES_DIR="$BATS_TEST_DIRNAME/.fakes"
CALLS_DIR="$FAKES_DIR/.calls"

export PATH="$FAKES_DIR:$PATH"

# $1 program name
# $2 expected arguments
# $3 output
fake() {
    if [ ! -d $FAKES_DIR ]; then
        mkdir $FAKES_DIR
        mkdir $CALLS_DIR
    fi
    echo $2 >> $CALLS_DIR/$1.expected
    if [ ! -f $FAKES_DIR/$1 ]; then
        cat <<EOF > $FAKES_DIR/$1
#!/usr/bin/env bash
echo \$@ >> $CALLS_DIR/$1.actual
echo $3
EOF
        chmod +x $FAKES_DIR/$1
    fi
}

cleanupfakes() {
    rm -rf $FAKES_DIR
}

verify() {
    $(cmp $CALLS_DIR/$1.expected $CALLS_DIR/$1.actual)
}
